SAFE Scraper
============

Scrapes the Bristol SAFE website to download all submitted assessments.

Dependencies
------------

* Python
* python-selenium
* geckodriver

Usage
-----

Enter your start year, end year, and uni credentials into config.cfg and execute
main.py. All assement files will be downloaded in folders named e.g.

```
2014-5/Programming and Algorithms I/Assessment C1: HelloWorld/hello.c
```

All courses across the specfied years with 'COMS' in their name will have their
assessment files downloaded.
