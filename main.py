import os
import sys
import configparser
import requests
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

MARKDOWN = False
try:
    from html2text import html2text
    MARKDOWN = True
except ImportError:
    print("Install html2text to get unit desciptions in markdown")

class Link():
    def __init__(self, url, parent=None, name=None):
        self.url = url
        self.name = name
        self.parent = parent

    def get_path(self):
        if not self.parent:
            return self.name
        if self.name:
            return os.path.join(self.parent.get_path(), self.name)
        return None

    def __eq__(self, other):
        return self.url == other.url

    def __ne__(self, other):
        return self.url != other.url

    def __hash__(self):
        return hash(self.url)

    def __repr__(self):
        string = ""
        if self.name:
            string += self.name + " "
        string += self.url
        return string

class Scraper():
    def __init__(self):
        self.driver = webdriver.Firefox()
        self.driver.set_window_size(1120, 550)
        self.username = None
        self.password = None
        # Initialise credentials
        self.get_credentials()
        # Declare variables
        self._course_links = None
        self._assessment_links = None
        self._file_links = None
        # Mapping links to Course name
        self._link_names = {}

    def get_credentials(self):
        if not self.username or not self.password:
            cfg = configparser.ConfigParser()
            cfg.read('config.cfg')
            self.username = cfg.get("Credentials", "username")
            self.password = cfg.get("Credentials", "password")
        return self.username, self.password

    def get_year_ints(self):
        cfg = configparser.ConfigParser()
        cfg.read('config.cfg')
        start_year = cfg.getint("Years", "first")
        end_year = cfg.getint("Years", "last")
        year_ints = [(i, i+1) for i in range(start_year, end_year)]
        return year_ints

    def get_year_strings(self):
        year_ints = self.get_year_ints()
        year_strings = ["%d-%s" % (i, str(j)[-1]) for (i, j) in year_ints]
        return year_strings

    def login(self):
        self.driver.get("https://sso.bris.ac.uk/sso/login")
        self.driver.find_element_by_id("username").send_keys(self.username)
        self.driver.find_element_by_id("password").send_keys(self.password)
        self.driver.find_element_by_id("submit").click()

    def get_course_links(self):
        if self._course_links:
            return
        year_strings = self.get_year_strings()
        year_urls = ["https://wwwa.fen.bris.ac.uk/%s/coms/%s/progress.jsp" \
                % (year_string, self.username) for year_string in year_strings]
        years = [Link(year_urls[i], name=year_strings[i]) \
                for i in range(len(year_strings))]
        links = []
        for year in years:
            self.driver.get(year.url)
            urls = [element.get_attribute("href")\
                    for element in self.driver.find_elements_by_partial_link_text('COMS')]
            for url in urls:
                link = Link(url, parent=year)
                links.append(link)
        self._course_links = links

    def get_assessment_links(self):
        if self._assessment_links:
            return
        if not self._course_links:
            self.get_course_links()
        links = []
        for course in self._course_links:
            self.driver.get(course.url)
            course.name = self.driver.find_element_by_css_selector("h2").text
            urls = [element.get_attribute("href")\
                    for element in self.driver.find_elements_by_link_text('SAFE')]
            for url in urls:
                link = Link(url, course)
                links.append(link)
        self._assessment_links = list(set(links))

    def get_file_links(self):
        if self._file_links:
            return
        if not self._assessment_links:
            self.get_assessment_links()
        links = []
        for assessment in self._assessment_links:
            self.driver.get(assessment.url)
            try:
                assessment.name = self.driver.find_element_by_css_selector("h2")\
                        .text[len(assessment.parent.name):].strip()
            except NoSuchElementException:
                pass
            urls = [element.get_attribute("href")\
                    for element in self.driver.find_elements_by_css_selector('tr a')]
            for url in urls:
                (_, filename) = os.path.split(url)
                link = Link(url, assessment, filename)
                links.append(link)
        self._file_links = links

    def download_all(self):
        if not self._file_links:
            self.get_file_links()
        # Setup session for downloading files
        session = requests.Session()
        cookies = self.driver.get_cookies()
        for cookie in cookies:
            session.cookies.set(cookie['name'], cookie['value'])
        # Iterate through files
        for link in self._file_links:
            file_path = link.get_path()
            print("%s -> %s ..." % (link.url, file_path), end='')
            sys.stdout.flush()
            folder_path = link.parent.get_path()
            if not os.path.exists(folder_path):
                os.makedirs(folder_path)
            response = session.get(link.url)
            with open(file_path, 'wb') as _file:
                _file.write(response.content)
            print("/")
        print("Download complete")

    def get_course_descriptions(self):
        template_url = \
            "https://www.bris.ac.uk/unit-programme-catalogue/UnitDetails.jsa?ayrCode=%s&unitCode=%s"
        for course in self._course_links:
            course_url = course.url
            if course_url[-1] == '/':
                course_url = course.url[:-1]
            (_, unit_code) = os.path.split(course_url)
            year_string = course.parent.name
            year_code = year_string[2:4] + "%2F" + str(int(year_string[2:4])+1)
            description_url = template_url % (year_code, unit_code)
            self.driver.get(description_url)
            container = self.driver.find_element_by_xpath("//table/parent::div")
            html = container.get_attribute("innerHTML")
            # Save to file
            folder_path = course.get_path()
            file_path = os.path.join(folder_path, "description.html")
            if not os.path.exists(folder_path):
                os.makedirs(folder_path)
            with open(file_path, 'w') as _file:
                _file.write(html)
            if MARKDOWN:
                file_path = os.path.join(folder_path, "description.md")
                markdown = html2text(html)
                with open(file_path, 'w') as _file:
                    _file.write(markdown)

    def quit(self):
        self.driver.quit()

    def run(self):
        self.login()
        print("Login complete...")
        self.get_course_links()
        print("Retrieved %d course links..." % len(self._course_links))
        self.get_assessment_links()
        print("Retrieved %d assessment links..." % len(self._assessment_links))
        self.get_course_descriptions()
        print("Retrieved course descriptions...")
        self.get_file_links()
        print("Retrieved %d file links..." % len(self._file_links))
        print("Download starting...")
        self.download_all()
        self.quit()

if __name__ == "__main__":
    scraper = Scraper()
    scraper.run()
